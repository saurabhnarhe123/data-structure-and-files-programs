#include <iostream>
#include <string>
using namespace std;

struct node {
    char data;
    node *left, *right;
};

struct stack {
    node *data;
    stack *next;
};

class Tree {

  public :

    stack *top;
    node *root;
    Tree() {
        root = NULL;
        top = NULL;
    }

    bool isOp(char ch) {
        if(ch == '+' || ch == '-' || ch == '*' || ch == '/' || ch == '%' || ch == '$' || ch == '^') {
            return true;
        } else {
            return false;
        }
    }

    stack* push(node *dt) {
        stack *temp = new stack ;
        temp -> data = dt ;
        if(top!=NULL) {
            temp -> next = top;
            top = temp;
        } else {
            temp -> next = NULL;
            top = temp;
        }
        return top;
    }

    node* pop() {
        node *temp;
        temp = top -> data;
        top = top -> next;
        return temp;
    }

     node* create() {

        char s[20];
        int i = 0;
        cout<<"\nEnter postfix expression: ";
        cin>>s;

        while(s[i] != '\0') {

            cout<<"\n"<<i;

            node *tmp = new node;
            tmp -> data = s[i];

            if(isOp(s[i])) {
                cout<<"\nIts operand, so pop 2 times\n";
                tmp -> right = pop();
                tmp -> left = pop();
                top = push(tmp);
                root = tmp;
            } else {
                cout<<"\nIts char, so push\n";
                tmp -> left = tmp -> right = NULL;
                top = push(tmp);
                root = tmp;
            }
            i++;
        }
        return root;
   }

     void inOrder(node *p) {
         if(p!=NULL) {
             inOrder(p->left);
             cout<<" "<<p->data;
             inOrder(p->right);
         }
     }
     void postOrder(node *p) {
         if(p!=NULL) {
             postOrder(p->left);
             postOrder(p->right);
             cout<<" "<<p->data;
         }
     }

     void preOrder(node *p) {
         if(p!=NULL) {
             cout<<" "<<p->data;
             preOrder(p->left);
             preOrder(p->right);
         }
     }

     void nonInOrder(node *root) {

         top=NULL;
         cout<<"\n\t Non-recursive";
         node *current,*tempnode;
         current=root;
         push(current);
         current=current->left;

         while(current!=NULL) {
             push(current);
             current=current->left;
         }
         while(top!=NULL) {
            if(current==NULL) {
                tempnode=pop();
                cout<<tempnode->data;
                current=tempnode->right;
                while(current!=NULL) {
                    push(current);
                    current=current->left;
                }
            }
           }
   }

     void nonPreOrder(node *p) {

     }

     void nonPostOrder(node *p) {

     }
};

int main() {

    int ch;
    int c= 1;
    Tree T;

    do {
        cout<<"\n1.Create Tree\n2.Recursive Inorder\n3.Recursive Postorder\n4.Recursive Preorder \n5.Non-Recursive Inorder \n6.Non-Recursive Preorder \n7.Non-Recursive Postorder";
        cout<<"\nEnter your choice:\t";
        cin>>ch;

        switch(ch) {

        case 1:
            T.root = T.create();
            break;

        case 2:
            T.inOrder(T.root);
            break;

        case 3:
            T.postOrder(T.root);
            break;

        case 4:
            T.preOrder(T.root);
            break;
        case 5:
            T.nonInOrder(T.root);
            break;
        case 6:
            T.nonPreOrder(T.root);
            break;
        case 7:
            T.nonPostOrder(T.root);
            break;
        default:
            cout<<"\nWrong Choice please try again...";
        }
    } while(c==1);
    return 0;
}
