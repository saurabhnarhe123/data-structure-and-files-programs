#include <iostream>
using namespace std;
struct edge
{
	int Start,End,Weight;
};
class Graph_SPT
{
public:
	int graph_Ad_mat[20][20];


	edge Sp_Table[30],Sp_Tree[30];
	  int Sp_Ver=0,Sp_TV=0;
	  int edges,Ver;
	  int parent[30];
	  void sort();
	  int find(int ver);
	  void Union(int v1,int v2);

	  char V_name[];
	void inputGraph();
	void Display_Mat();
	void mini_Kruskal();
	//int check(char S[10]);
};
void Graph_SPT::inputGraph()
{
	int i,j;
	cout<<"\nEnter Total number of Vertices :";
	cin>>Ver;
	cout<<"\nEnter Number of Edges U Have in ur Graph :";
	cin>>edges;

	for(i=0;i<Ver;i++)
	{
		cout<<"\n Enter Names of Vertex "<<i+1<<":";
		cin>>V_name[i];
	}
	for(i=0;i<Ver;i++)//initialize adjacency Matrix
	{
		for(j=0;j<Ver;j++)
		{
			if(i==j)
			graph_Ad_mat[i][j]=0;
			else
				graph_Ad_mat[i][j]=32767;
		}
	}
	char temp;
	for(i=0;i<edges;i++)
	{
	cout<<"\nEnter Start vertex of Edge "<<i+1<<": ";
	cin>>temp;
	int S=-1,E=-1; //initialize start and End
	j=0;
	 while(j<Ver && S==-1)//to avoid duplication of edges
	 {
		 if(V_name[j]==temp)
		 {
		      	 S=j;
		     cout<<"\nEnter End of Edge "<<i+1<<":";
		     cin>>temp;
		     j=0;
		     while(j<Ver && E==-1)
		     {
		       if(V_name[j]==temp)
		       {
		    	   E=j;
		    	   if(graph_Ad_mat[S][E]==32767 && graph_Ad_mat[E][S]==32767)
		    	   {
		    		   cout<<"\nEnter weight of the Edge : "<<V_name[S]<<"-"<<V_name[E]<<":";
		    		    cin>>graph_Ad_mat[S][E];  //enter graph_Ad_mat of edge in adjacency matrix
		    		    graph_Ad_mat[E][S]=graph_Ad_mat[S][E];//undirected graph

		    		    //sparse table creation
                   Sp_Table[Sp_Ver].Start=S;
                   Sp_Table[Sp_Ver].End=E;
                   Sp_Table[Sp_Ver].Weight=graph_Ad_mat[S][E];
                   Sp_Ver++;

		    	   }
		    	   else
		    	   {
		    		   cout<<"\nEdge Already Present ";
		    		   j=Ver;
		    		   i--; //decrement to take edge again
		    	   }
		       }
		       j++;
		     }
		 }
		 j++;
	 }
	 if(S==-1 || E==-1)//if wrong vertex name given
	 {
	 cout<<"\nThere is NO such  of Vertex... ";
	 i--;
	 }
	}
	cout<<"\nGraph Created ";

}

void Graph_SPT::Display_Mat()
{
	int i,j;
	cout<<"\nAdjacency Matrix of Given Graph is :";
	cout<<"\n  ";
	for(i=0;i<Ver;i++)
		cout<<V_name[i]<<"\t";
	for(i=0;i<Ver;i++)
	{
		cout<<"\n"<<V_name[i]<<"|";
		for(j=0;j<Ver;j++)
		{

			cout<<graph_Ad_mat[i][j]<<"\t";
		}
		cout<<"|";
	}

}
void Graph_SPT::sort()
{
	int i,j;
	edge temp;
	cout<<"\n \n";
	cout<<"\nSparse table Before Sorting:";
	cout<<"\nStart \t End \t Weight";
		for(j=0;j<Sp_Ver;j++)
		{
			cout<<"\n"<<Sp_Table[j].Start<<"\t "<<Sp_Table[j].End<<"\t "<<Sp_Table[j].Weight;
		}

	for(i=1;i<Sp_Ver;i++)
	{
		for(j=0;j<Sp_Ver-i;j++)
		{
			if(Sp_Table[j].Weight>Sp_Table[j+1].Weight)
			{
				temp=Sp_Table[j];
				Sp_Table[j]=Sp_Table[j+1];
				Sp_Table[j+1]=temp;
			}
		}
	}

	cout<<"\n\nSparse table After Sort:";
		cout<<"\nStart \t End \t Weight";
			for(j=0;j<Sp_Ver;j++)
			{
				cout<<"\n"<<Sp_Table[j].Start<<"\t "<<Sp_Table[j].End<<"\t "<<Sp_Table[j].Weight;
			}
}

int Graph_SPT::find(int ver)
{
	return parent[ver];
}

void Graph_SPT::Union(int v1,int v2)
{
	int i;
	for(i=0;i<Ver;i++)
	{
		if(parent[i]==v2)
			parent[i]=v1;
	}
}
void Graph_SPT::mini_Kruskal()
{
	int i,pv1,pv2,cost=0;
  sort();
  for(i=0;i<Ver;i++)
  {
	  parent[i]=i;
  }
  for(i=0;i<Sp_Ver;i++)
  {
	  pv1=find(Sp_Table[i].Start);
	  pv2=find(Sp_Table[i].End);
	  if(pv1!=pv2)
	  {
       Sp_Tree[Sp_TV]=Sp_Table[i];
       Sp_TV++;
       Union(pv1,pv2);
	  }
  }

cout<<"\nBy Kruskal's Algorithm";

  cout<<"\nStart \t End \t Weight";
  for(i=0;i<Sp_TV;i++)
  {
	cout<<"\n"<<Sp_Tree[i].Start<<"\t "<<Sp_Tree[i].End<<"\t "<<Sp_Tree[i].Weight;
  cost=cost+Sp_Tree[i].Weight;
  }

  cout<<"\nCost of Miminum Spanning tree Is : "<<cost;
}
int main()
{
    Graph_SPT GS;
    GS.inputGraph();
    cout<<"\nYour Adjacency Matrix :";
    GS.Display_Mat();
     GS.mini_Kruskal();
    return 0;
}
