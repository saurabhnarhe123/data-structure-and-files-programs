//============================================================================
// Name        : exp10PrimsAlgo.cpp
// Author      : 
// Version     :
// Copyright   : Your copyright notice
// Description : A business house has several offices in different contrays they want to leave line to connect them with each other and the phone comapny charges different rent to connect diff pais of cities business house want to connect all its ofifices with min cost solve this issue uising adjacency matrix and prims algo.
//============================================================================

#include <iostream>
#include <string.h>
#define MAX 10
using namespace std;

class Graph {
public :

	struct City {
		string name ;
		int index ;
	};
	int M[MAX][MAX];
	City C[MAX];

	Graph () {
		for(int i=0;i<MAX;i++) {
			C[i].index = i;
			for(int j=0;j<MAX;j++) {
				M[i][i] = 0;
			}
		}
	}

    void create(int);
    void insertNode(char cityName[]);
    void insertArc();
    void prims();
    void disp();


};

void Graph::create(int n) {
	int i,  numOfLinks, c;
	int sourceIndex = -1;
	int destIndex = -1;
	string sourceCity, destCity;

	for(i=0;i<n;i++) {
		cout<<"\nEnter name of the city : \t";
		cin>>C[i].name;
	}

	for(i=0;i<n;i++) {
		cout<<"\nName = "<<C[i].name<<"\nIndex = "<<C[i].index;
	}

	cout<<"\n\nHow many links do you want to create ? :\t";
	cin>>numOfLinks;

	for(c=0;c<numOfLinks;c++) {
		cout<<"\nEnter name of the source city and destination city : \t";
		cin>>sourceCity>>destCity;

		for(i=0;i<n;i++) {
			if(C[i].name == sourceCity) {
				sourceIndex = i;
			}
			if(C[i].name == destCity) {
				destIndex = i;
			}
		}
		if(sourceIndex == -1 || destIndex == -1) {
			cout<<"\n That city dosent exist...";
			numOfLinks++;
			continue;
		}
		cout<<"\nsource Index = "<<sourceIndex<<"\nDest = "<<destIndex;

		cout<<"\nEnter the cost :\t ";
		cin>>M[sourceIndex][destIndex];
		M[destIndex][sourceIndex] = M[sourceIndex][destIndex];
	}

	cout<<"\nCreated...\n";
	for(i=0;i<n;i++) {
		cout<<"\n";
		for(int j=0;j<n;j++) {
			cout<<"\t"<<M[i][j];
		}
	}
}


void Graph::prims() {

}

void Graph::disp() {

}

int main() {

    int ch, c=1, n;
    Graph G;

    do {

        cout<<"\n\n1. Create a graph\n2. Apply Prims Algo \n3. Display \nEnter your choice : \t";
        cin>>ch;

        switch(ch) {
        case 1:
        	cout<<"\nEnter number of cities : \t";
        	cin>>n;
            G.create(n);
            break;
        case 2:
            break;
        case 3:
            G.disp();
            break;
        case 4:
            break;
        default :
            cout<<"\nWrong choice, please try again...";
        }

    } while(c==1);

    return 0;
}
