//============================================================================
// Name        : exp7-KruskalAlgo.cpp
// Author      : Saurabh Narhe
// Version     :
// Copyright   : Your copyright notice
// Description : Represent a real world graph using adjacency list and find minimum spanning tree using
//				 Krsukals Algo.
//============================================================================

#include <iostream>
#include <string.h>
using namespace std;

class Graph {
public :
	struct graphArc;
	struct graphNode;
	struct graphNode {
		graphNode *nextNode;
		char cityName[20];
		graphArc *nextArc;
	};

	struct graphArc {
		graphArc *nextArc;
		graphNode *destination;
		int weight;
	};

	graphNode *start;

	Graph() {
		start = NULL;
	}

	void create();
	void insertNode(char cityName[]);
	void insertArc();
	void kruskal();
	void disp();


};

void Graph::create() {
	int n, i;
	char name[20];

	cout<<"\nHow many cities you want to insert? : \t";
	cin>>n;
	for(i=0;i<n;i++) {
		cout<<"\nEnter name of the city "<<i<<" : \t";
		cin>>name;
		insertNode(name);
	}

	cout<<"\nHow many edges do you want to have ? :\t";
	cin>>n;
	for(i=0;i<n;i++) {
		insertArc();
	}
}



void Graph::insertArc() {
	graphNode *s, *d;
	s = d = start;
	char sourceCity[20], destCity[20];
	cout<<"\nEnter source city and destination city\n";
	cin>>sourceCity>>destCity;

	while(s!=NULL) {
		if( strcmp(s->cityName, sourceCity) )
			break;
		s = s -> nextNode;
	}
	if(s == NULL) {
		cout<<"\nSource city dos'nt exist...";
		return;
	}
	while(d!=NULL) {
		if( strcmp(d->cityName, destCity) )
			break;
		d = d -> nextNode;
	}
	if(d == NULL) {
		cout<<"\nDestination city dos'nt exist...";
		return;
	}

	int wt;
	cout<<"\nEnter weight of the arc : \t";
	cin>>wt;
	if(s->nextArc == NULL) {
		graphArc *newArc = new graphArc;
		newArc -> weight = wt;
		newArc -> nextArc = NULL;
		newArc -> destination = d;
	} else {
		graphArc *p = s -> nextArc;
		while(p != NULL) {
			p = p -> nextArc;
		}
		p  = new graphArc;
		p -> nextArc = NULL;
		p -> destination = d;
		p -> weight = wt;
	}
}

void Graph::insertNode(char cityName[]) {
		if(start==NULL) {
			start = new graphNode;
			strcpy(start -> cityName, cityName);
			start -> nextNode = NULL;
			start -> nextArc = NULL;
		} else {
			graphNode *newNode = new graphNode;
			newNode -> nextArc = NULL;
			newNode -> nextNode = NULL;
			strcpy(newNode -> cityName, cityName);
			start -> nextNode = newNode;
		}
}

void Graph::kruskal() {

}

void Graph::disp() {
	graphArc *a;
	graphNode *p;
	p = start;
	while(p!=NULL) {
		cout<<"\n"<<p->cityName;
		 a = p -> nextArc ;
		while(a!=NULL) {
			cout<<" -> "<<a -> destination -> cityName<<"("<<a->weight<<")";
			a = a -> nextArc;
		}
		p = p -> nextNode;
	}
}

int main() {

	int ch, c=1;
	Graph G;

	do {

		cout<<"\n\n1. Create a graph\n2. Apply Kruskal Algo \n3. Display \nEnter your choice : \t";
		cin>>ch;

		switch(ch) {
		case 1:
			G.create();
			break;
		case 2:
			break;
		case 3:
			G.disp();
			break;
		case 4:
			break;
		default :
			cout<<"\nWrong choice, please try again...";
		}

	} while(c==1);

	return 0;
}
