//============================================================================
// Name        : exp8.cpp
// Author      : Saurabh Narhe
// Version     : 1.1
// Copyright   : Your copyright notice
// Description : Represent a given graph using adjacency matrix and
//				 find shortest path using dijkstras algorithm (single source all destination)
//============================================================================

#include <iostream>
#include <string.h>
using namespace std;
#define infinity 999

class Graph {
public :
	int cost[10][10];
	char vertex[10][10];
	int nodes, sourceVertex, predecessor[10], distance[10], flag[10], closestNode;

	Graph() {
		int i,j;
		for(i=0;i<10;i++) {
			for(j=0;j<10;j++) {
				if(i==j) {
					cost[i][j] = 0;
				} else {
					cost[i][j] = infinity;
				}
			}
		}
		nodes = sourceVertex = closestNode =  0;
	}

	void create() {


		char start[10], end[10], ch;
		int i, j, wt;

		cout<<"\nEnter number of nodes : ";
		cin>>nodes;

		for(i=0;i<nodes;i++) {
			cout<<"\nEnter vertex name : ";
			cin>>vertex[i];
		}

		do {
			cout<<"\nEnter start and end vertex : ";
			cin>>start>>end;
			cout<<"\nEnter weight :";
			cin>>wt;
			i = position(start);
			j = position(end);
			cost[i][j] = cost[j][i] = wt;
			cout<<"\nDo you want to insert more edges ?";
			cin>>ch;
		}while(ch=='y' || ch=='Y');

		cout<<"\nEnter source vertex : ";
		cin>>sourceVertex;

		while(sourceVertex < 0 || sourceVertex > nodes-1) {
			cout<<"'\nEnter source vertex between 0 and "<<(nodes-1)<<" : ";
			cin>>sourceVertex;
		}
	}

	int position(char s[]) {

		int i;
		for(i=0;i<nodes;i++) {
			if(strcmp(vertex[i], s) == 0) {
				break;
			}
		}
		return i;
	}

	void disp() {

		int i, j;

		for(i=0;i<nodes;i++)
			cout<<"\t "<<i;

		for(i=0;i<nodes;i++) {
			cout<<"\n"<<i;
			for(j=0;j<nodes;j++) {
				if(cost[i][j] == infinity) {
					cout<<"\t~";
				}
				cout<<"\t"<<cost[i][j];
			}
		}
	}
	void init() {
		int i;
		for(i=0;i<nodes;i++) {
			flag[i] = false;
			distance[i] = infinity;
			predecessor[i] = -1;
		}
		distance[sourceVertex] = 0;
	}

	int getClosestNode() {
		int minDistance = infinity, i;
		for(i=0;i<nodes;i++) {
			if(!flag[i] && minDistance >=distance[i] ) {
				minDistance = distance[i];
				closestNode = i;
			}
		}
		return closestNode;
	}

	void dijkstras() {
		int count = 0, i;
		init();
		while(count < nodes) {
			closestNode = getClosestNode();
			flag[closestNode] = true;
			for(i=0;i<nodes;i++) {
				if(!flag[i] && cost[closestNode]>0) {
					if(distance[i] > (distance[closestNode]+cost[closestNode][i])) {
						distance[i] = distance[closestNode]+cost[closestNode][i];
						predecessor[i] = closestNode;
					}
				}
			}
			count++;
		}
	}
};

int main() {

	int ch, c = 1;
	Graph G;
	do {
		cout<<"\n1.Create a graph\n2.\n3.";
		cin>>ch;

		switch(ch) {
			case 1:
				G.create();
				break;
			case 2:
				G.disp();
				break;
			case 3:closestNode
				break;
			default:
				cout<<"\nWrong choice please try again...";
		}
	} while(c==1);

	return 0;
}
