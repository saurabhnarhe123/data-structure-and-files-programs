//============================================================================
// Name       : exp9.cpp
// Author      : Saurabh Narhe
// Version     : 1.0
// Copyright   : This program is only for learning purpose...
// Description :   Store data of students with telephone numbers and names in the structure
//					 	using hashing function
//						for telephone number implement chaining with and without replacement
//============================================================================
#include <iostream>
#include <string.h>
using namespace std;
#define MAX 10

struct hash {
	char name[20];
	long int tele;
	int chain;
}rec[MAX];

class hashTable {
public:
	hashTable() {
		for(int i=0;i<MAX;i++) {
			strcpy(rec[i].name,"-");
			rec[i].tele = 0;
			rec[i].chain = -1;
		}
	}

	int hashing(long int tel) {
		return (tel%MAX);
	}
	void chainingWithReplacement(struct hash h);
	void chainingWithoutReplacement(struct hash h);
	void disp();

};

void hashTable::chainingWithoutReplacement(struct hash h) {
	int k = hashing(h.tele);
	int p = k;
	int i;
	if(rec[k].tele == 0) {
		rec[k].tele = h.tele;
		strcpy(rec[k].name, h.name);
	} else {
		i = k + 1;
		do {
			if(hashing(rec[i].tele) == k) {
				p = k;
			}
			if(rec[i].tele == 0) {
				rec[i].tele = h.tele;
				strcpy(rec[i].name, h.name);
				if(hashing(rec[p].tele) == k) {
					rec[p].chain = i;
				}
				break;
			}
			i++;
		} while(i%MAX != k);
		if(i == k) {
			cout<<"\nSorry, could'nt insert new record. Hash Table is full... ";
		}
	}
}

void hashTable::disp() {
	cout<<"\n\n";
	cout<<"\n\tINDEX         TELEPHONE          CHAIN";
	for(int i=0; i<MAX; i++) {
		if(rec[i].tele == 0) {
			cout<<"\n\t  "<<i<<"           "<<rec[i].tele<<"                  "<<rec[i].chain;
		} else {
		cout<<"\n\t  "<<i<<"           "<<rec[i].tele<<"         "<<rec[i].chain;
		}
	}
}

void hashTable::chainingWithReplacement(struct hash h) {
	int k = hashing(h.tele);
	int p = k;
	int i, change_pos;
	if(rec[k].tele == 0) {
		rec[k].tele = h.tele;
		strcpy(rec[k].name, h.name);
	} else if(hashing(rec[k].tele) == hashing(h.tele)) {
		i = k + 1;
		do {
			if(hashing(rec[i].tele) == k) {
				p = k;
			}
			if(rec[i].tele == 0) {
				rec[i].tele = h.tele;
				strcpy(rec[i].name, h.name);
				if(hashing(rec[p].tele) == k) {
					rec[p].chain = i;
				}
				break;
			}
			i++;
		} while(i%MAX != k);
		if(i == k) {
			cout<<"\nSorry, could'nt insert new record. Hash Table is full... ";
		}
	} else {
		hash temp;
		temp.tele = rec[k].tele;
		strcpy(temp.name,rec[k].name);
		rec[k].tele = h.tele;
		strcpy(rec[k].name, h.name);
		i = k + 1;
		do {
			if(rec[i].tele == 0) {
				rec[i].tele = temp.tele;
				strcpy(rec[i].name, temp.name);
				change_pos = i;
			}
			i++;
		} while(i%MAX != k);

		for(i=0;i<MAX;i++) {
			if(rec[i].chain == k) {
				rec[i].chain = change_pos;
				break;
			}
		}
	}
}

int main() {
	int ch, c=1;
	hashTable ht;
	hash h;

	do {
		cout<<"\n\n1.Chaining without replacement \n2.Chaining with replacement " ;
		cin>>ch;

		switch(ch) {

			case 1:
				cout<<"\nEnter name :\t";
				cin>>h.name;
				cout<<"\nEnter telephone number :\t";
				cin>>h.tele;
				ht.chainingWithoutReplacement(h);
				ht.disp();
				break;

			case 2:
				cout<<"\nEnter name :\t";
				cin>>h.name;
				cout<<"\nEnter telephone number :\t";
				cin>>h.tele;
				ht.chainingWithReplacement(h);
				ht.disp();
				break;
		}
	}while(c==1);

	return 0;
}
