//============================================================================
// Name        : Stack.cpp
// Author      : Saurabh Narhe
// Version     : 1.0
// Copyright   : This progrqam is in public domain
// Description : Implement stack as an ADT using Linked List and
//				 use this ADT for conversion of infix expression to postfix, prefix and
//				 evaluation of postfix, prefix expression also
//				 use this ADT for conversion of postfix to infix & prefix to infix expression.
//============================================================================

#include <iostream>
using namespace std;

template<class T>
class Stack {

		struct node {
			T data;
			struct node *next;
		};
		struct node *top;

public :

		Stack() {
			top = NULL;
		}

		void push(T x);

		T pop();

		int precedence(char ch){
			if(ch=='(')
				return 0;
			if(ch=='+' || ch=='-')
				return -1;
			if(ch=='*' || ch=='/' || ch=='%')
				return 2;
			return 3;
		}

		bool isEmpty(){
			if(top == NULL)
				return true;
			else
				return false;
		}

		bool isFull() {
			node *temp;
			temp = new node;
			if(temp != NULL) {
				delete temp;
				return false;
			} else {
				return true;
			}
		}

		void disp();

		T getTop();

		~Stack() {
			node *temp;
			while(top!=NULL) {
				temp = top;
				top = top -> next;
				delete temp;
			}
		}

		string infixToPostfix(string inStr);
		string infixToPrefix(string inStr);
		int postfixEvaluate(string inStr);
};


template<class T>
void Stack<T>::push(T x) {
	node *newPtr;
	newPtr = new node;
	if(isEmpty()) {
		newPtr -> data = x;
		newPtr -> next = NULL;
		top = newPtr;
	} else {
		newPtr -> data = x;
		 newPtr -> next = top;
		 top = newPtr ;
		top = newPtr;
	}
}

template<class T>
T Stack<T>::pop() {
		node *temp;
		int x;
		temp = top;
		x = top->data;
		top = top -> next;
		delete temp;
		return x;
}

template<class T>
void Stack<T>::disp() {
	node *p = top;
	while(p!=NULL) {
		cout<<"\n\t"<<p->data;
		p = p->next;
	}
}

template<class T>
T Stack<T>::getTop() {
		return top->data;
}

template<class T>
string Stack<T>::infixToPostfix(string inStr) {

	Stack <char> st;
	string poStr(" "), opStr;
	char ch, op;
	for(int i=0; i<inStr.length(); i++) {
		ch = inStr.at(i);
		if( (ch>='a' && ch<='z') || (ch>='A' && ch<='Z') ) {
			poStr.append(1,ch);
		} else {
			if( ch=='(' ) {
				st.push(ch);
			} else if( ch==')' ) {
				while( (op=st.pop()) != '(' ) {
					poStr.append(1,op);
				}
			} else if(st.isEmpty()!=true && st.precedence(ch) <= st.precedence(st.getTop())) {
				op = st.pop();
				poStr = poStr.append(1,op);
			}
			if( ch!= '(' && ch!=')' ) {
				st.push(ch);
			}
		}
	}

	while(st.isEmpty()!=true) {
		poStr.append(1, st.pop());
	}

	return poStr;
}
//incomplete
template<class T>
string Stack<T>::infixToPrefix(string inStr) {
	Stack<char> st;
	string revStr, preStr, poStr;
	char ch;
	for(int i=0; i<inStr.length(); i++) {
		st.push(inStr.at(i));
	}
	while(!st.isEmpty()) {
		ch = st.pop();
		if( ch=='(' )
			revStr.append(1,')');
		if( ch==')' )
			revStr.append(1,'(');
		revStr.append(1,ch);
	}
	poStr = infixToPostfix(revStr);

}
//incomplete
template<class T>
int Stack<T>::postfixEvaluate(string inStr) {

}

int main() {

	Stack <int>S1;
	Stack <char>S2;
	int ch, exit, intData, charData, type, no;
	string str;
	cout<<"\nWhat type of data you want to enter in stack?\nPress 1 - Integer \nPress 2 - Character\n";
	cin>>type;

	do {

		cout<<"\n1.Push onto stack \n2.Pop from stack \n3.Display \n4.Get top element \n5.Infix to Postfix conversion \n6.Infix to Prefix conversion";
		cout<<" \n7.Exit \nEnter your choice...\n";
		cin>>ch;

		switch(ch) {
		case 1:
			cout<<"\nHow many elements do you want to insert?\n";
			cin>>no;
			if(type == 1) {
				while(no>0) {
					if(!S1.isFull()) {
						cout<<"\nEnter data to insert:\n";
						cin>>intData;
						S1.push(intData);
					} else {
						cout<<"\nStack is full...";
					}
					no--;
				}
			} else {
				while(no>0) {
					if(!S2.isFull()) {
						cout<<"\nEnter data to insert:\n";
						cin>>charData;
						S2.push(charData);
					} else {
						cout<<"\nStack is full...";
					}
					no--;
				}
			}
			break;
		case 2:
			cout<<"\n How many elements do you want to pop?\n";
			cin>>no;
			if(type==1)
				while(no>0) {
					if(S1.isEmpty()) {
						cout<<"\nStack is empty\n";
					}else{
						cout<<"\n"<<S1.pop()<<" deleted from stack...\n";
					}
					no--;
				}
			else
				while(no>0) {
					if(S2.isEmpty())
						cout<<"\nStack is empty";
					else
						cout<<"\n"<<S2.pop()<<" deleted from stack...\n";
					no--;
				}
			break;
		case 3:
			if(type==1) {
				if(S1.isEmpty()) {
					cout<<"\nStack is empty...";
				} else {
					cout<<"\n...Displaying elements in the stack...";
					S1.disp();
				}
			} else {
				if(S2.isEmpty()) {
					cout<<"\nStack is empty...";
				} else {
					cout<<"\n...Displaying elements in the stack...";
					S2.disp();
				}
			}
			break;
		case 4:
			if(type==1)
				if(S1.isEmpty())
					cout<<"\nStack is empty";
				else
					cout<<S1.getTop();
			else
				if(S2.isEmpty())
					cout<<"\nStack is empty";
				else
					cout<<S2.getTop();
			break;

			case 5:
					cout<<"\n\nEnter infix expression to convert : \n";
					cin>>str;
					cout<<"\nPostfix is: \t"<<S1.infixToPostfix(str);
				break;

			case 6:
				cout<<"\nEnter expression to convert:\t";
				cin>>str;
				cout<<"\nPrefix is: \t"<<S1.infixToPrefix(str);
				break;
			case 7:
				exit=1;
				break;
		default:
			 cout<<"\nWrong choice, Please try again...";
		}

	}while(exit!=1);

	return 0;
}
